from urllib.request import urlopen
from urllib.parse import quote_plus
from bs4 import BeautifulSoup  
from selenium import webdriver
import time


# id = "ksohee7074"
# pw = "qkqh0521"
# keyword = '오크'
# keyword2 = '카푸'
# keyword3 = '치노'
# keyword4 = '베이지'
# cafeUrl = 'https://cafe.naver.com/campingkan'
# boardPath = '//*[@id="menuLink27"]'
# replyMessage = '1'
# isCheckLastBoard = True
	

# ■■오크돔M 카푸치노 색상 ■■
id = "odw2934"
pw = "ehdnjs2934"
keyword = '오크'
keyword2 = '카푸'
keyword3 = '치노'
keyword4 = '베이지'
keyword5 = '종료'
cafeUrl = 'https://cafe.naver.com/devoh'
boardPath = '//*[@id="menuLink1"]'
replyMessage = '1'
isCheckLastBoard = True

refreshInterval = 0.001
replyInterval = 0.1

# Initialize the driver
driver = webdriver.Chrome()

driver.get(cafeUrl)


try:
    driver.find_element_by_xpath('//*[@id="gnb_login_button"]').click()
except:
    print('not login button')

time.sleep(0.5)
driver.execute_script("document.getElementsByName('id')[0].value = \'" + id + "\'")
time.sleep(1)
driver.execute_script("document.getElementsByName('pw')[0].value=\'" + pw + "\'")
time.sleep(1)

# 매크로 탐지를 회피하기 위한 자바스크립트 사용
driver.find_element_by_xpath('//*[@id="log.login"]').click()

time.sleep(1)
driver.find_element_by_xpath(boardPath).click()
time.sleep(1)

driver.switch_to.frame('cafe_main') # 프레임을 게시판 프레임으로 변경
blockFrom1 = '//*[@id="main-area"]/div[4]/table/tbody/tr['
blockTo1 = ']/td[1]/div[2]/div/a[1]'

blockFrom2 = '//*[@id="main-area"]/div[4]/table/tbody/tr['
blockTo2 = ']/td[1]/div[3]/div/a'


# xpath 를 조립하기 위한 경로의 앞, 뒷부분

article = "gnr.title"
count = 1        # 게시글 카운트

success = True # while 문의 작동을 구분할 bool 변수

# 시작시 글 목록 수
pageString = driver.page_source
bsObj = BeautifulSoup(pageString, 'html.parser')

lastBoardNumber = bsObj.find_all(class_="inner_number")[len(bsObj.find_all(class_="inner_number")) - 1].getText()

while success:
    pageString = driver.page_source
    bsObj = BeautifulSoup(pageString, 'html.parser')
    title = bsObj.find_all(class_="article")
    refreshedLastBoardNumber = bsObj.find_all(class_="inner_number")[len(bsObj.find_all(class_="inner_number")) - 1].getText()
    time.sleep(refreshInterval)
    
    #페이지가 새로고침될 때 마다 페이지 정보를 받아와 title에 저장
    for i in title:
        if article in i['onclick']:
            print(i.getText().strip())
            text = i.getText().strip()

            changedBoard = refreshedLastBoardNumber != lastBoardNumber
            # 게시글목록이 변경된 조건을 반영할지 여부
            if(isCheckLastBoard == False):
                changedBoard = True

            if (changedBoard == True):
                hasArticleRow = False
                try:
                    xpath = blockFrom1 + str(count) + blockTo1
                    driver.find_element_by_xpath(xpath).click()
                    hasArticleRow = True
                except:
                    print('게시글 1패드 못찾음.')
                try:
                    xpath = blockFrom2 + str(count) + blockTo2
                    driver.find_element_by_xpath(xpath).click()
                    hasArticleRow = True
                except:
                    print('게시글 2패드 못찾음.')

                success=False
                
                hasTextarea = False
                hasRegisterBtn = False

                while hasTextarea == False:
                    # time.sleep(replyInterval)

                    try:
                        driver.find_element_by_xpath('//*[@id="app"]/div/div/div[2]/div[2]/div[5]/div[3]/div[1]/textarea').send_keys(replyMessage)
                        hasTextarea = True
                        print("1번째 댓글창을 찾음.")
                        break
                    except:
                        print("1번째 댓글창을 못찾음.")
                    try:
                        driver.find_element_by_xpath('//*[@id="app"]/div/div/div[2]/div[2]/div[5]/div[2]/div[1]/textarea').send_keys(replyMessage)
                        hasTextarea = True
                        print("2번째 댓글창을 찾음.")
                        break
                    except:
                        print("2번째 댓글창을 못찾음.")
                    try:
                        driver.find_element_by_xpath('//*[@id="app"]/div/div/div[2]/div[2]/div[4]/div[2]/div[1]/textarea').send_keys(replyMessage)
                        hasTextarea = True
                        print("3번째 댓글창을 찾음.")
                        break
                    except:
                        print("3번째 댓글창을 못찾음.")


                while hasRegisterBtn == False:
                    try:
                        # driver.find_element_by_xpath('//*[@id="app"]/div/div/div[2]/div[2]/div[5]/div[2]/div[2]/div[2]/a').click()
                        hasRegisterBtn = True
                        print("1번째 댓글 등록버튼 찾음.")
                        break
                    except:
                        print("1번째 댓글 등록버튼 못찾음.")
                    try:
                        # driver.find_element_by_xpath('//*[@id="app"]/div/div/div[2]/div[2]/div[5]/div[3]/div[2]/div[2]/a').click()
                        hasRegisterBtn = True
                        print("2번째 댓글 등록버튼 찾음.")
                        break
                    except:
                        print("2번째 댓글 등록버튼 못찾음.")
                    try:
                        # driver.find_element_by_xpath('//*[@id="app"]/div/div/div[2]/div[2]/div[4]/div[2]/div[2]/div[2]/a').click()
                        hasRegisterBtn = True
                        print("3번째 댓글 등록버튼 찾음.")
                        break
                    except:
                        print("3번째 댓글 등록버튼 못찾음.")

                break
                #조건에 맞는 게시물을 찾으면 접속하고 success를 false로 변경
            count+=1
    count=1
    if success:
        driver.switch_to.parent_frame()
        driver.find_element_by_xpath(boardPath).click()
        driver.switch_to.frame('cafe_main')
        # 찾지 못했을 경우 새로고침 대신 다시 게시판에 들어오는 과정을 거침

time.sleep(10)
driver.quit()


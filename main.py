import tkinter as tk
import tkinter.scrolledtext as st
from selenium import webdriver
import naverLogin
import util
import os
import sys


def login():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--incognito")
    driver = webdriver.Chrome(options=chrome_options, executable_path="chromedriver")

    ok = naverLogin.login(driver, txtNaverId.get(), txtNaverPW.get())
    if(ok):
        util.moveToCafeArticle(driver, txtCafeId.get(), txtMenuId.get())
        driver.implicitly_wait(1)
        util.refreshArticleList(driver, True, txtInterval.get(), txtCafeId.get(), txtMenuId.get(), txtkeyword.get())
        util.writeReply(driver, txtReplyMsg.get())


window = tk.Tk()

window.title = "네이버 카페 댓글 등록"
window.geometry("840x400+100+100")


lblNaverId = tk.Label(window, text="네이버 아이디")
lblNaverId.pack()
lblNaverId.grid(row = 0, column=0)

lblNaverPW = tk.Label(window, text="네이버 비밀번호")
lblNaverPW.pack()
lblNaverPW.grid(row = 1, column=0)

txtNaverId = tk.Entry(window, width=20)
txtNaverId.pack()
txtNaverId.grid(row = 0, column = 1)

txtNaverPW = tk.Entry(window, width=20)
txtNaverPW.pack()
txtNaverPW.grid(row = 1, column = 1)

btnLogin = tk.Button(window, text="로그인", command = login)
btnLogin.pack()
btnLogin.grid(row = 4, column = 0)

lblCafeId = tk.Label(window, text="카페아이디")
lblCafeId.pack()
lblCafeId.grid(row = 0, column=4)

txtCafeId = tk.Entry(window, width=20)
txtCafeId.pack()
txtCafeId.grid(row = 0, column = 5)

lblMenuId = tk.Label(window, text="카페메뉴아이디")
lblMenuId.pack()
lblMenuId.grid(row = 1, column=4)

txtMenuId = tk.Entry(window, width=20)
txtMenuId.pack()
txtMenuId.grid(row = 1, column = 5)

lblReplyMsg = tk.Label(window, text="댓글내용")
lblReplyMsg.pack()
lblReplyMsg.grid(row = 2, column=0)

txtReplyMsg = tk.Entry(window, width=20)
txtReplyMsg.pack()
txtReplyMsg.grid(row = 2, column = 1)

lblInterval = tk.Label(window, text="새로고침간격(초 ex:0.1)")
lblInterval.pack()
lblInterval.grid(row = 2, column = 4)

txtInterval = tk.Entry(window, width=20)
txtInterval.pack()
txtInterval.grid(row = 2, column = 5)

lblkeyword = tk.Label(window, text="검색어 콤마로 구분 ex)카푸치노,오크돔")
lblkeyword.pack()
lblkeyword.grid(row = 3, column = 0)

keywored = tk.StringVar()
txtkeyword = tk.Entry(window, width=20, textvariable = keywored)
txtkeyword.pack()
txtkeyword.grid(row = 3, column = 1) 

interval = "0"


window.mainloop()

def main():
    print('')


if __name__ == '__main__':
    main()


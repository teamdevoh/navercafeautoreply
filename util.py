from selenium import webdriver
from bs4 import BeautifulSoup
import time

def moveToCafeArticle(driver, cafeId, menuId):
    driver.get('https://m.cafe.naver.com/ca-fe/web/cafes/'+ cafeId +'/menus/' + menuId)

    
def refreshArticleList(driver, enabled, interval, cafeId, menuId, keyword):
    oldFirstArticle = ''
     
    while enabled:
        hasTabMenuButton = False
        while hasTabMenuButton == False:
            try:
                button = driver.find_element_by_xpath('//*[@id="app"]/div/div/div[3]/div/div/ul/li[1]/a')
                button.click()
                hasTabMenuButton = True
            except:
                print('탭 메뉴 버튼을 못찾음.')
        
        time.sleep(float(interval))
        articleUrls = []
        
        articleList = driver.find_elements_by_css_selector('li.board_box a.txt_area')
        for index, article in enumerate(articleList):
            if(oldFirstArticle != '' and index == 0 and oldFirstArticle != article.get_attribute('href')):
                
                isMatch = False

                if(len(keyword) > 0):
                    keywords = keyword.split(',')
                    title = article.text.split('\n')[1]
                    for keyword in keywords:
                        if(title.find(keyword) > -1):
                            isMatch = True
                else:
                    isMatch = True

                if(isMatch):
                    print('새로운 글을 감지함.')
                    articleHref = article.get_attribute('href')

                    start = articleHref.find('articleid=') + len('articleid=')
                    end = articleHref.find('&boardtype=')
                    articleId = articleHref[start:end]
                    
                    driver.get('https://m.cafe.naver.com/ca-fe/web/cafes/'+ cafeId +'/articles/'+articleId+'/comments?fromList=true&menuId=' + menuId)
                    enabled = False
                    break

            if(index == 0 and oldFirstArticle == ''):
                oldFirstArticle = article.get_attribute('href')
            articleUrls.append(article.get_attribute('href'))
            
        print(len(articleUrls))

def writeReply(driver, msg):
    hasReplyBox = False
    hasRegisterButton = False

    while hasReplyBox == False:
        try:
            textarea = driver.find_element_by_css_selector('div.CommentWriteArea__inbox > textarea.isDeactivatedUser')
            textarea.click()
            textarea.send_keys(msg)
            hasReplyBox = True
        except:
            print('댓글창을 찾지못함')
    
    while hasRegisterButton == False:
        try:
            registerButton = driver.find_element_by_css_selector('button.CommentWriteUpload__btn_register')
            registerButton.click()
            hasRegisterButton = True
        except:
            print('등록버튼을 찾지못함')

    time.sleep(5)            
    driver.quit()
    
    
